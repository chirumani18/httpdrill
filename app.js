const express = require("express");
const path = require("path");
const uuid = require("uuid");

const app = express();

const string = {
    "slideshow": {
        "author": "Yours Truly",
        "date": "date of publication",
        "slides": [
            {
                "title": "Wake up to WonderWidgets!",
                "type": "all"
            },
            {
                "items": [
                    "Why <em>WonderWidgets</em> are great",
                    "Who <em>buys</em> WonderWidgets"
                ],
                "title": "Overview",
                "type": "all"
            }
        ],
        "title": "Sample Slide Show"
    }
};



app.get('/html', (request, response) => {
    response.sendFile(path.join(__dirname, 'getHtmlPro1.html'));
});



app.get('/json', (request, response) => {
    response.json(string);
})


app.get('/uuid', (request, response) => {
    const uniqId = uuid.v4();
    const uniqIdObj = {
        "uuid": uniqId,
    }
    response.send(uniqIdObj);
})


app.get('/status/:statusCode', (request, response) => {
    const code = request.params.statusCode;
    response.send(`Return a response with ${code} status code`);
})


app.get('/delay/:delayTime', (request, response) => {
    const delay = request.params.delayTime;
    response.setHeader('200', { "Content-Type": 'text/plain' });
    setTimeout(() => {
        response.send(`Response after ${delay} seconds`);
    }, delay * 1000);
})


app.listen(5000);
